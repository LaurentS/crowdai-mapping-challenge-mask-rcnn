#!/usr/bin/env python
import json
import numpy as np

import crowdai_helpers
import traceback

import glob
import os

# specific imports
import coco
import skimage
from mrcnn import model as modellib
from pycocotools import mask as maskutils


"""
Expected ENVIRONMENT Variables

* CROWDAI_TEST_IMAGES_PATH : abs path to  folder containing all the test images
* CROWDAI_PREDICTIONS_OUTPUT_PATH : path where you are supposed to write the output predictions.json
"""

# Configuration Variables
IMAGE_WIDTH = 300
IMAGE_HEIGHT = 300
SEGMENTATION_LENGTH = 10
MAX_NUMBER_OF_ANNOTATIONS = 10

# paths
ROOT_DIR = os.getcwd()
MODEL_DIR = os.path.join(ROOT_DIR, "logs")
# copy weights in the same folder as this file
MODEL_PATH = os.path.join(ROOT_DIR, "submitted_weights.h5")

# global vars :(
config = None
model = None


def gather_images(test_images_path):
    images = glob.glob(os.path.join(
        test_images_path, "*.jpg"
    ))
    return images


def get_image_path(image_id):
    test_images_path = os.getenv("CROWDAI_TEST_IMAGES_PATH", False)
    return os.path.join(test_images_path, str(image_id).zfill(12))


def gather_input_output_path():
    test_images_path = os.getenv("CROWDAI_TEST_IMAGES_PATH", False)
    assert test_images_path is not False, "Provide path to test images using the env var CROWDAI_TEST_IMAGES_PATH"

    predictions_output_path = os.getenv("CROWDAI_PREDICTIONS_OUTPUT_PATH", False)
    assert predictions_output_path is not False, "Provide output path in CROWDAI_PREDICTIONS_OUTPUT_PATH"

    return test_images_path, predictions_output_path


def register_progress(image_id):
    ########################################################################
    # Register Prediction
    #
    # Note, this prediction register is not a requirement. It is used to
    # provide you feedback of how far are you in the overall evaluation.
    # In the absence of it, the evaluation will still work, but you
    # will see progress of the evaluation as 0 until it is complete
    #
    # Here you simply announce that you completed processing a set of
    # image-ids
    ########################################################################
    crowdai_helpers.execution_progress({
        "image_ids": [image_id]
    })


def register_success(predictions_output_path):
    ########################################################################
    # Register Prediction Complete
    ########################################################################
    crowdai_helpers.execution_success({
        "predictions_output_path": predictions_output_path
    })


class InferenceConfig(coco.CocoConfig):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    GPU_COUNT = 1
    IMAGES_PER_GPU = 4
    NUM_CLASSES = 1 + 1  # 1 Background + 1 Building
    IMAGE_MAX_DIM = 320
    IMAGE_MIN_DIM = 320
    NAME = "crowdai-mapping-challenge"


def configure_model():
    config = InferenceConfig()
    # config.display()
    return config


def setup_model(config):
    model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR, config=config)

    model_path = MODEL_PATH
    model.load_weights(model_path, by_name=True)

    return model


def get_batches(images_path, config):
    files = gather_images(images_path)
    all_batches = []
    _buffer = []
    batch_length = config.IMAGES_PER_GPU * config.GPU_COUNT
    for _idx, _file in enumerate(files):
        if len(_buffer) == batch_length:
            all_batches.append(_buffer)
            _buffer = []
        else:
            _buffer.append(_file)

    # skip up to 3 images at the end, as it is really insignificant if we miss them
    # if len(_buffer) > 0:
        # we have less than a full batch, pad it to batch_length so that keras doesn't complain
        # _buffer += _buffer[-1] * (batch_length - len(_buffer))
        # all_batches.append(_buffer)

    return all_batches


def predict_all_batches(model, all_batches, predictions_output_path):
    # Iterate over all the batches and predict
    _final_object = []
    for files in all_batches:
        images = [skimage.io.imread(x) for x in files]
        predictions = model.detect(images, verbose=0)
        for _idx, r in enumerate(predictions):
            _file = files[_idx]
            image_id = int(_file.split("/")[-1].replace(".jpg", ""))
            register_progress(image_id)
            for _idx, class_id in enumerate(r["class_ids"]):
                if class_id == 1:
                    mask = r["masks"].astype(np.uint8)[:, :, _idx]
                    bbox = np.around(r["rois"][_idx], 1)
                    bbox = [float(x) for x in bbox]
                    _result = {}
                    _result["image_id"] = image_id
                    _result["category_id"] = 100
                    _result["score"] = float(r["scores"][_idx])
                    _mask = maskutils.encode(np.asfortranarray(mask))
                    _mask["counts"] = _mask["counts"].decode("UTF-8")
                    _result["segmentation"] = _mask
                    _result["bbox"] = [bbox[1], bbox[0], bbox[3] - bbox[1], bbox[2] - bbox[0]]
                    _final_object.append(_result)

    fp = open(predictions_output_path, "w")
    fp.write(json.dumps(_final_object))
    fp.close()


def run():
    # Register Prediction Start
    crowdai_helpers.execution_start()

    config = configure_model()
    model = setup_model(config)

    # Gather Input and Output paths from environment variables
    test_images_path, predictions_output_path = gather_input_output_path()
    print(test_images_path)
    print(predictions_output_path)

    all_batches = get_batches(test_images_path, config)
    predict_all_batches(model, all_batches, predictions_output_path)

    register_success(predictions_output_path)


if __name__ == "__main__":
    try:
        run()
    except Exception as e:
        error = traceback.format_exc()
        print(error)
        crowdai_helpers.execution_error(error)
