import math
from .model import data_generator
import keras
from keras.callbacks import LambdaCallback
from matplotlib import pyplot as plt
############################################################
#  Learning rate finder
############################################################

# Example usage:
# from mrcnn.extras import LRFinder
# lrf = LRFinder(model, dataset_train)
# lrf.find_lr(batch_size=4)

# then plot results with
# from matplotlib import pyplot as plt
# lrf.plot_loss(n_skip_beginning=10, n_skip_end=5, smoothing=0.99)


class LRFinder:
    def __init__(self, model, dataset):
        self.model = model
        self.dataset = dataset
        self.average_loss = 0.
        self.best_loss = 10.
        self.beta = 0.9

        # outputs
        self.losses = []
        self.learning_rates = []

    def on_batch_end(self, batch, logs):
        """called at the end of each batch, store loss, update lr"""
        # Log the learning rate
        lr = self.model.keras_model.optimizer.lr
        self.learning_rates.append(lr)

        # Log the loss
        loss = logs['loss']
        self.losses.append(loss)

        # Check whether the loss got too large or NaN
        if math.isnan(loss) or loss > self.best_loss * 4:
            self.model.keras_model.stop_training = True
            return

        if loss < self.best_loss:
            self.best_loss = loss

        # Increase the learning rate for the next batch
        lr *= self.lr_multiplier
        self.model.keras_model.optimizer.lr = lr

    def find_lr(self, first_lr=1e-8, last_lr=10., batch_size=2, momentum=0.9):
        """ Find the optimum learning rate
            """
        num_batches = len(self.dataset.image_ids) / batch_size
        self.lr_multiplier = (last_lr / first_lr) ** (1/num_batches)

        # momentum of 0 is the same as basic SGD
        self.model.compile(first_lr, momentum)

        original_lr = self.model.keras_model.optimizer.lr  # backup lr so we can restore it
        self.model.keras_model.save_weights('lr_finder_tmp.h5')

        self.model.keras_model.optimizer.lr = first_lr

        # save weights before we start messing with them so we can restore them at the end

        data_gen = data_generator(self.dataset, self.model.config, shuffle=True,
                                  augmentation=None,
                                  batch_size=batch_size)

        callback = LambdaCallback(on_batch_end=lambda batch, logs: self.on_batch_end(batch, logs))

        self.model.keras_model.fit_generator(
            data_gen,
            steps_per_epoch=num_batches,
            initial_epoch=self.model.epoch,
            epochs=self.model.epoch + 1,
            callbacks=[callback],
            verbose=2,
            use_multiprocessing=False
        )

        # restore original settings
        self.model.keras_model.optimizer.lr = original_lr
        self.model.keras_model.load_weights('lr_finder_tmp.h5')
        print('done!')

    def plot_loss(self, n_skip_beginning=10, n_skip_end=5, smoothing=0.9):
        """
        Plots the loss.
        Parameters:
            n_skip_beginning - number of batches to skip on the left.
            n_skip_end - number of batches to skip on the right.
            smoothing - float between 0 and 1. How much to smooth the curve. 0 means no smoothing.
        """
        ys = []
        avg_loss = 0
        for batch, l in enumerate(self.losses):
            avg_loss = smoothing * avg_loss + (1 - smoothing) * l
            ys.append(avg_loss / (1 - smoothing ** batch))

        plt.ylabel("loss")
        plt.xlabel("learning rate (log scale)")
        plt.plot(self.learning_rates[n_skip_beginning:-n_skip_end], ys[n_skip_beginning:-n_skip_end])
        plt.xscale('log')

    def plot_loss_change(self, sma=1, n_skip_beginning=10, n_skip_end=5, y_lim=(-0.01, 0.01)):
        """
        Plots rate of change of the loss function.
        Parameters:
            sma - number of batches for simple moving average to smooth out the curve.
            n_skip_beginning - number of batches to skip on the left.
            n_skip_end - number of batches to skip on the right.
            y_lim - limits for the y axis.
        """
        assert sma >= 1
        derivatives = [0] * sma
        for i in range(sma, len(self.lrs)):
            derivative = (self.losses[i] - self.losses[i - sma]) / sma
            derivatives.append(derivative)

        plt.ylabel("rate of loss change")
        plt.xlabel("learning rate (log scale)")
        plt.plot(self.lrs[n_skip_beginning:-n_skip_end], derivatives[n_skip_beginning:-n_skip_end])
        plt.xscale('log')
        plt.ylim(y_lim)


class OneCycleTrainer():

    def __init__(self, model, train_dataset, val_dataset):
        self.model = model
        self.train_dataset = train_dataset
        self.val_dataset = val_dataset

    def on_batch_end(self, batch, logs):
        self.model.keras_model.optimizer.lr += self.lr_delta
        self.model.keras_model.optimizer.momentum += self.momentum_delta
        print('New params: LR: {} | momentum: {}'.format(
                self.model.keras_model.optimizer.lr,
                self.model.keras_model.optimizer.momentum
                ))

    def train(self, total_epochs=40, tail_epochs=10, lr_low=0.001, lr_high=0.1, momentum_low=0.8, momentum_high=0.9):

        part_epochs = total_epochs // 2

        self.model.compile(lr_low, momentum_high)

        # part 1: raise learning rate while lowering momentum
        self.lr_delta = (lr_high - lr_low) / part_epochs
        self.momentum_delta = (momentum_low - momentum_high) / part_epochs

        train_data_gen = data_generator(self.train_dataset, self.model.config, shuffle=True,
                                  augmentation=None,
                                  batch_size=self.model.config.BATCH_SIZE)
        val_data_gen = data_generator(self.val_dataset, self.model.config, shuffle=True,
                                  augmentation=None,
                                  batch_size=self.model.config.BATCH_SIZE)

        callbacks = [
            keras.callbacks.TensorBoard(log_dir=self.model.log_dir,
                                        histogram_freq=0, write_graph=True, write_images=False),
            keras.callbacks.ModelCheckpoint(self.model.checkpoint_path,
                                            verbose=0, save_weights_only=True),
            LambdaCallback(on_batch_end=lambda batch, logs: self.on_batch_end(batch, logs)),
            ]

        # train all layers but the backbone
        self.model.set_trainable(r"(mrcnn\_.*)|(rpn\_.*)|(fpn\_.*)")

        self.model.keras_model.fit_generator(
            train_data_gen,
            validation_data=val_data_gen,
            steps_per_epoch=self.model.config.STEPS_PER_EPOCH,
            validation_steps=self.model.config.VALIDATION_STEPS,
            initial_epoch=self.model.epoch,
            epochs=self.model.epoch + part_epochs,
            callbacks=callbacks,
            verbose=2,
            use_multiprocessing=False
        )

        # part 2: lower learning rate and increase momentum
        self.lr_delta = -self.lr_delta
        self.momentum_delta = -self.momentum_delta

        self.model.keras_model.fit_generator(
            train_data_gen,
            validation_data=val_data_gen,
            steps_per_epoch=self.model.config.STEPS_PER_EPOCH,
            validation_steps=self.model.config.VALIDATION_STEPS,
            initial_epoch=self.model.epoch,
            epochs=self.model.epoch + part_epochs,
            callbacks=callbacks,
            verbose=2,
            use_multiprocessing=False
        )

        # part 3: drop learning rate a bit further, keeping momentum
        # drop lr to 1/10th of it's final value over 10% of the total epochs
        self.lr_delta = -(lr_low * 0.9) / (total_epochs / 10)
        self.momentum_delta = 0

        self.model.keras_model.fit_generator(
            train_data_gen,
            validation_data=val_data_gen,
            steps_per_epoch=self.model.config.STEPS_PER_EPOCH,
            validation_steps=self.model.config.VALIDATION_STEPS,
            initial_epoch=self.model.epoch,
            epochs=self.model.epoch + (total_epochs / 10),
            callbacks=callbacks,
            verbose=2,
            use_multiprocessing=False
        )
        print('done!')
